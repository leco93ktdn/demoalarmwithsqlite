package com.example.alarmclockdemo.notifi_service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import androidx.core.app.NotificationManagerCompat;

import com.example.alarmclockdemo.common.Common;
import com.example.alarmclockdemo.database.DataBaseHelper;
import com.example.alarmclockdemo.model.AlarmModel;

public class NotificationActionService extends IntentService {

    public NotificationActionService() {
        super(NotificationActionService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        DataBaseHelper db = new DataBaseHelper(getApplicationContext());
        String action = intent.getAction();
        if ("ACTION_CANCEL".equals(action) || "ACTION_RESTART".equals(action)) {
            int id = intent.getIntExtra("ID", 0);
            AlarmModel model = db.getAlarm(id);
            if (model != null) {
                Common.stopMediaPlayer();
                Common.cancelAlarm(getApplicationContext(), id);
                NotificationManagerCompat.from(this.getApplicationContext()).cancel(id);
//            NotificationManagerCompat.from(this.getApplicationContext()).cancelAll();
                if (model.getRepeatType().equals("Never")) {
                    model.setFlag(0);
                    db.updateAlarm(model);
                }

                if (model.getFlag() == 1) {
                    Common.setAlarm(getApplicationContext(), model.getId());
                } else {
                    Common.cancelAlarm(getApplicationContext(), model.getId());
                }

                Intent intentBroadcast = new Intent("REFRESH_SWITCH");
                getApplicationContext().sendBroadcast(intentBroadcast);
            }
        }
    }
}