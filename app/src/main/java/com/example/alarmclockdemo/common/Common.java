package com.example.alarmclockdemo.common;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.alarmclockdemo.R;
import com.example.alarmclockdemo.activity.CreateAlarmActivity;
import com.example.alarmclockdemo.database.DataBaseHelper;
import com.example.alarmclockdemo.model.AlarmModel;
import com.example.alarmclockdemo.alarm_receiver.AlarmReceiver;
import com.example.alarmclockdemo.notifi_service.NotificationActionService;

import java.io.IOException;
import java.util.Calendar;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import static android.content.Context.ALARM_SERVICE;
import static android.content.Context.NOTIFICATION_SERVICE;

public class Common {
    private static MediaPlayer mediaPlayer;
    private static DataBaseHelper db;

    private static Uri getAlarmUri() {

        Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);

        if (alert == null) {

            alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            if (alert == null) {

                alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            }
        }
        return alert;
    }

    public static void setAlarm(Context context, int alarmId) {
        db = new DataBaseHelper(context);
        AlarmModel data = db.getAlarm(alarmId);
        Calendar calNow = Calendar.getInstance();
        Calendar calAlam = parseAlarm(data);

        if (calAlam.compareTo(calNow) <= 0) {
            if (data.getRepeatType().equals("Year")) {
                while (calAlam.compareTo(calNow) <= 0) {
                    calAlam.add(Calendar.YEAR, data.getRepeatTime());
                }
                data = updateAlarm(context, alarmId, calAlam);
            } else {
                calAlam = (Calendar) calNow.clone();
                calAlam.set(Calendar.SECOND, 0);
                calAlam.set(Calendar.MILLISECOND, 0);
                switch (data.getRepeatType()) {
                    case "Never":
                        calAlam.set(Calendar.HOUR_OF_DAY, data.getHour());
                        calAlam.set(Calendar.MINUTE, data.getMinute());
                        if (data.getHour() < calNow.get(Calendar.HOUR_OF_DAY)
                                || (data.getHour() == calNow.get(Calendar.HOUR_OF_DAY) && data.getMinute() < calNow.get(Calendar.MINUTE))) {
                            calAlam.add(Calendar.DAY_OF_MONTH, 1);
                        }
                        data = updateAlarm(context, alarmId, calAlam);
                        break;
                    case "Minute":
                        calAlam.set(Calendar.MINUTE, data.getMinute());
                        while (calAlam.compareTo(calNow) <= 0) {
                            calAlam.add(Calendar.MINUTE, data.getRepeatTime());
                        }
                        data = updateAlarm(context, alarmId, calAlam);
                        break;
                    case "Hour":
                        calAlam.set(Calendar.MINUTE, data.getMinute());
                        calAlam.set(Calendar.HOUR_OF_DAY, data.getHour());
                        while (calAlam.compareTo(calNow) <= 0) {
                            calAlam.add(Calendar.HOUR_OF_DAY, data.getRepeatTime());
                        }
                        data = updateAlarm(context, alarmId, calAlam);
                        break;
                    case "Day":
                        calAlam.set(Calendar.DAY_OF_MONTH, data.getDayOfMonth());
                        calAlam.set(Calendar.HOUR_OF_DAY, data.getHour());
                        calAlam.set(Calendar.MINUTE, data.getMinute());
                        while (calAlam.compareTo(calNow) <= 0) {
                            calAlam.add(Calendar.DAY_OF_MONTH, data.getRepeatTime());
                        }
                        data = updateAlarm(context, alarmId, calAlam);
                        break;
                    case "Week":
                        calAlam.set(Calendar.DAY_OF_MONTH, data.getDayOfMonth());
                        calAlam.set(Calendar.HOUR_OF_DAY, data.getHour());
                        calAlam.set(Calendar.MINUTE, data.getMinute());
                        while (calAlam.compareTo(calNow) <= 0) {
                            calAlam.add(Calendar.DAY_OF_MONTH, 7 * data.getRepeatTime());
                        }
                        data = updateAlarm(context, alarmId, calAlam);
                        break;
                    case "Month":
                        calAlam.set(Calendar.MONTH, data.getMonth());
                        calAlam.set(Calendar.DAY_OF_MONTH, data.getDayOfMonth());
                        calAlam.set(Calendar.HOUR_OF_DAY, data.getHour());
                        calAlam.set(Calendar.MINUTE, data.getMinute());
                        while (calAlam.compareTo(calNow) <= 0) {
                            calAlam.add(Calendar.MONTH, data.getRepeatTime());
                        }
                        data = updateAlarm(context, alarmId, calAlam);
                        break;
                }
            }
        }
        startAlarm(context, data, calAlam);
    }

    private static AlarmModel updateAlarm(Context context, int alarmId, Calendar calAlam) {
        AlarmModel newModel = new AlarmModel();
        newModel.setId(alarmId);
        newModel.setYear(calAlam.get(Calendar.YEAR));
        newModel.setMonth(calAlam.get(Calendar.MONTH));
        newModel.setDayOfMonth(calAlam.get(Calendar.DAY_OF_MONTH));
        newModel.setHour(calAlam.get(Calendar.HOUR_OF_DAY));
        newModel.setMinute(calAlam.get(Calendar.MINUTE));
        db = new DataBaseHelper(context);
        db.updateAlarm(newModel);
        return newModel;
    }

    private static void startAlarm(Context context, AlarmModel data, Calendar calAlam) {
        Intent intent = new Intent(context.getApplicationContext(), AlarmReceiver.class);
        intent.setAction("ACTION_ALARM");
        intent.putExtra("KEY_TONE_URL", getAlarmUri().toString());
        intent.putExtra("ID", data.getId());
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), data.getId(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getApplicationContext().getSystemService(ALARM_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //MARSHMALLOW OR ABOVE
            Log.e("SDK VERSION", "MARSHMALLOW");
            Log.e("ALARM AT:", calAlam.get(Calendar.YEAR) + "/" + calAlam.get(Calendar.MONTH) + "/" +
                    calAlam.get(Calendar.DAY_OF_MONTH) + " - " + calAlam.get(Calendar.HOUR_OF_DAY) + ":" + calAlam.get(Calendar.MINUTE) + ":"
                    + calAlam.get(Calendar.SECOND) + " - Repeat: " + data.getRepeatTime() + " " + data.getRepeatType());

            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,
                    calAlam.getTimeInMillis(),
                    pendingIntent);

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //LOLLIPOP 21 OR ABOVE
            Log.d("SDK VERSION", "LOLLIPOP");
            AlarmManager.AlarmClockInfo alarmClockInfo = new AlarmManager.AlarmClockInfo(calAlam.getTimeInMillis(), pendingIntent);
            alarmManager.setAlarmClock(alarmClockInfo, pendingIntent);

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //KITKAT 19 OR ABOVE
            Log.d("SDK VERSION", "KITKAT");
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, calAlam.getTimeInMillis(), pendingIntent);

        } else {
            //FOR BELOW KITKAT ALL DEVICES
            Log.d("LeCo", "ALL DEVICES");
            alarmManager.set(AlarmManager.RTC_WAKEUP, calAlam.getTimeInMillis(), pendingIntent);
        }
    }

    private static Calendar parseAlarm(AlarmModel data) {
        Calendar calAlam = Calendar.getInstance();
        calAlam.set(Calendar.YEAR, data.getYear());
        calAlam.set(Calendar.MONTH, data.getMonth());
        calAlam.set(Calendar.DAY_OF_MONTH, data.getDayOfMonth());
        calAlam.set(Calendar.HOUR_OF_DAY, data.getHour());
        calAlam.set(Calendar.MINUTE, data.getMinute());
        calAlam.set(Calendar.SECOND, 0);
        calAlam.set(Calendar.MILLISECOND, 0);
        return calAlam;
    }

    public static void startMediaPlayer(Context context, String uriString) {
        mediaPlayer = new MediaPlayer();
        try {
            final AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
            if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC); //can change volume
                mediaPlayer.setDataSource(context.getApplicationContext(), Uri.parse(uriString));
                mediaPlayer.prepare();
                mediaPlayer.start();
            }
        } catch (IOException e) {
            Log.e("EXCEPTION", e.toString());
        }
    }

    public static void stopMediaPlayer() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    public static void cancelAlarm(Context context, int id) {
        Intent intent = new Intent(context.getApplicationContext(), AlarmReceiver.class);
        intent.setAction("ACTION_ALARM");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), id, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getApplicationContext().getSystemService(ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);

    }

    public static void showNotification(Context context, int id) {
        db = new DataBaseHelper(context.getApplicationContext());
        AlarmModel model = db.getAlarm(id);
        if (model != null) {
            String title = String.format("%02d", model.getHour()) + ":" + String.format("%02d", model.getMinute());

            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel("YOUR_CHANNEL_ID",
                        "" + "YOUR_CHANNEL_NAME",
                        NotificationManager.IMPORTANCE_HIGH);
                channel.setDescription("YOUR_NOTIFICATION_CHANNEL_DISCRIPTION");
                mNotificationManager.createNotificationChannel(channel);
            }

//        intent change alarm
            Intent snoozeIntent = new Intent(context.getApplicationContext(), CreateAlarmActivity.class);
            snoozeIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            snoozeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            snoozeIntent.putExtra("MODEL", model);
            snoozeIntent.putExtra("KEY", "UPDATE");
            PendingIntent snoozePendingIntent =
                    PendingIntent.getActivity(context.getApplicationContext(), id,
                            snoozeIntent, 0);

//        intent cancel alarm
            Intent cancelIntent = new Intent(context.getApplicationContext(), NotificationActionService.class);
            cancelIntent.setAction("ACTION_CANCEL");
            cancelIntent.putExtra("ID", id);
            PendingIntent cancelPendingIntent =
                    PendingIntent.getService(context.getApplicationContext(), id,
                            cancelIntent, PendingIntent.FLAG_ONE_SHOT);

//        intent repeat alarm
            Intent restartIntent = new Intent(context.getApplicationContext(), NotificationActionService.class);
            restartIntent.setAction("ACTION_RESTART");
            restartIntent.putExtra("ID", id);
            PendingIntent restartPendingIntent =
                    PendingIntent.getService(context.getApplicationContext(), id,
                            cancelIntent, PendingIntent.FLAG_ONE_SHOT);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context.getApplicationContext(), "YOUR_CHANNEL_ID")
                    .setSmallIcon(R.drawable.ic_alarm) // notification icon
                    .setContentTitle(title) // title for notification
                    .setContentText(model.getMessage() + " ID: " + id)// message for notification
                    .setAutoCancel(true) // clear notification after click
                    .setOngoing(true)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(model.getMessage()))
//                .addAction(new NotificationCompat.Action(R.drawable.ic_alarm_off,"Restart", restartPendingIntent))
                    .addAction(new NotificationCompat.Action(R.drawable.ic_alarm, "Cancel", cancelPendingIntent))
                    .setContentIntent(snoozePendingIntent);
            mNotificationManager.notify(id, mBuilder.build());
            mBuilder.getNotification();
        }
    }
}
