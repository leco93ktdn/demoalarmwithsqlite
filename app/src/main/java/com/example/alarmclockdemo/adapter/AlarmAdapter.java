package com.example.alarmclockdemo.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.alarmclockdemo.R;
import com.example.alarmclockdemo.activity.CreateAlarmActivity;
import com.example.alarmclockdemo.activity.MainActivity;
import com.example.alarmclockdemo.common.Common;
import com.example.alarmclockdemo.database.DataBaseHelper;
import com.example.alarmclockdemo.model.AlarmModel;

import java.util.List;

public class AlarmAdapter extends RecyclerView.Adapter<AlarmAdapter.MyViewHolder> {
    private Activity context;
    private List<AlarmModel> data;
    private OnItemClickListener mClickListener;
    private DataBaseHelper db;
    private boolean checkBoxShow;
    private int REQUEST_CODE_CREATE_ALARM = 2000;

    public AlarmAdapter(Activity context, List<AlarmModel> data, boolean checkBoxShow) {
        this.context = context;
        this.data = data;
        this.checkBoxShow = checkBoxShow;
        db = new DataBaseHelper(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_alarm, parent, false);
        return new MyViewHolder(view);
    }

    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final AlarmModel model = data.get(position);
        holder.ryTimeItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mClickListener.onItemAdapterClick(model);
                settingAlarm(model);
            }
        });
        holder.tvTimeItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mClickListener.onItemAdapterClick(model);
                settingAlarm(model);
            }
        });
        holder.tvMessagetem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mClickListener.onItemAdapterClick(model);
                settingAlarm(model);
            }
        });
//        show checkbox
        if (checkBoxShow) {
            holder.chkDeleteCheck.setVisibility(View.VISIBLE);
        } else {
            holder.chkDeleteCheck.setVisibility(View.GONE);
        }
//        set checked
        if (model.getDeleteCheck() == 1) {
            holder.chkDeleteCheck.setChecked(true);
        } else {
            holder.chkDeleteCheck.setChecked(false);
        }

        holder.chkDeleteCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.chkDeleteCheck.isChecked()) {
                    model.setDeleteCheck(1);
                } else {
                    model.setDeleteCheck(0);
                }
                db.updateAlarm(model);
            }
        });

        if (model.getFlag() == 1) {
            holder.swStart.setChecked(true);
        } else {
            holder.swStart.setChecked(false);
        }

        holder.swStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.swStart.isChecked()) {
                    model.setFlag(1);
                    db.updateAlarm(model);
                    Common.setAlarm(context.getApplicationContext(), model.getId());
                } else {
                    model.setFlag(0);
                    db.updateAlarm(model);
                    Common.cancelAlarm(context.getApplicationContext(), model.getId());
                }
                Intent intent = new Intent("REFRESH_SWITCH");
                context.sendBroadcast(intent);
            }
        });

        holder.tvTimeItem.setText(String.format("%02d", model.getHour())
                + ":" + String.format("%02d", model.getMinute()));
        holder.tvDateItem.setText(String.format("%02d", model.getDayOfMonth())
                + "/" + String.format("%02d", model.getMonth() + 1) + "/" + model.getYear());
        holder.tvMessagetem.setText(model.getMessage());
        holder.tvTimeItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingAlarm(model);
            }
        });
    }

    private void settingAlarm( AlarmModel model) {
        Intent intent = new Intent(context, CreateAlarmActivity.class);
        intent.putExtra("MODEL", model);
        intent.putExtra("KEY", "UPDATE");
        context.startActivityForResult(intent, REQUEST_CODE_CREATE_ALARM);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvTimeItem, tvDateItem, tvMessagetem;
        Switch swStart;
        CheckBox chkDeleteCheck;
        LinearLayout lyTimeItem, ryItem;
        RelativeLayout lyRelative, ryTimeItem;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ryItem = itemView.findViewById(R.id.ryItem);
            lyRelative = itemView.findViewById(R.id.lyRelative);
            ryTimeItem = itemView.findViewById(R.id.ryTimeItem);
            lyTimeItem = itemView.findViewById(R.id.lyTimeItem);
            tvTimeItem = itemView.findViewById(R.id.tvTimeItem);
            tvDateItem = itemView.findViewById(R.id.tvDateItem);
            tvMessagetem = itemView.findViewById(R.id.tvMessagetem);
            swStart = itemView.findViewById(R.id.swStart);
            chkDeleteCheck = itemView.findViewById(R.id.chkDeleteCheck);
            this.setIsRecyclable(false);
        }
    }

    // allows clicks events to be caught
    public void setClickListener(OnItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    //parent activity will implement this method to respond to click events
    public interface OnItemClickListener {
        void onItemAdapterClick(AlarmModel model);
    }
}
