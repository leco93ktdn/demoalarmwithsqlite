package com.example.alarmclockdemo.dialog;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.alarmclockdemo.R;

public class CustomRepeatDialog extends DialogFragment {
    private Dialog dialog;
    private Activity activity;
    private Context context;
    private Spinner spinner;
    private EditText edtNumber;
    private Button btnChoose;
    private OnMyDialogResult dialogResult;
    private TextView tvTimeType;
    private String tag;

    public CustomRepeatDialog() {
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_custom_repeat_dialog, container, false);
        activity = this.getActivity();
        context = this.getActivity();
        dialog = this.getDialog();
        initView(view);
        initEvent();
        return view;
    }

    private void initView(View view) {
        spinner = view.findViewById(R.id.spChooseTimeType);
        edtNumber = view.findViewById(R.id.edtNumber);
        tvTimeType = view.findViewById(R.id.tvTimeType);
        btnChoose = view.findViewById(R.id.btnChoose);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(activity,
                R.array.snipper_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    private void initEvent() {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tag = spinner.getSelectedItem().toString();
                switch (tag) {
                    case "Minute":
                        tvTimeType.setText("phút");
                        break;
                    case "Hour":
                        tvTimeType.setText("giờ");
                        break;
                    case "Day":
                        tvTimeType.setText("ngày");
                        break;
                    case "Week":
                        tvTimeType.setText("tuần");
                        break;
                    case "Month":
                        tvTimeType.setText("tháng");
                        break;
                    case "Year":
                        tvTimeType.setText("năm");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btnChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogResult.finish((Integer.parseInt(edtNumber.getText().toString())), tag);
                dialog.dismiss();
            }
        });
    }

    public void setDialogResult(OnMyDialogResult result) {
        dialogResult = result;
    }

    public interface OnMyDialogResult {
        void finish(int time, String tag);
    }
}
