package com.example.alarmclockdemo.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;

import com.example.alarmclockdemo.R;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class CalendarDialog extends DialogFragment {

    private DatePicker date;
    private Button btnCancel, btnChoose;
    private Dialog dialog;
    private Activity activity;
    private Context context;
    OnMyDialogResult dialogResult;
    String dayOfMonth;
    String month;
    int year;

    public CalendarDialog() {

    }

    @Nullable
    @Override

    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.calendar_dialog, container, false);
        activity = this.getActivity();
        context = this.getActivity();
        dialog = this.getDialog();
        initView(view);
        initEvent();
        return view;
    }

    private void initEvent() {

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        btnChoose.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @SuppressLint("DefaultLocale")
            @Override
            public void onClick(View v) {

                dayOfMonth = String.format("%02d", date.getDayOfMonth());
                month = String.format("%02d", (date.getMonth() + 1));
                year = date.getYear();
                String dateParse = year + "-" + month + "-" + dayOfMonth;
                LocalDate convert = LocalDate.parse(dateParse);
                DayOfWeek dayOfWeek = convert.getDayOfWeek();
                String day = "";

                if (String.valueOf(dayOfWeek).equals("MONDAY")) day = "T.2";
                else if (String.valueOf(dayOfWeek).equals("TUESDAY")) day = "T.3";
                else if (String.valueOf(dayOfWeek).equals("WEDNESDAY")) day = "T.4";
                else if (String.valueOf(dayOfWeek).equals("THURSDAY")) day = "T.5";
                else if (String.valueOf(dayOfWeek).equals("FRIDAY")) day = "T.6";
                else if (String.valueOf(dayOfWeek).equals("SATURDAY")) day = "T.7";
                else if (String.valueOf(dayOfWeek).equals("SUNDAY")) day = "CN";
                String dateText = (day + ", " + dayOfMonth + "Th" + month);
                if (dialogResult != null) {
                    dialogResult.finish(dateText, date.getDayOfMonth(), (date.getMonth()), year);
                }
                dialog.dismiss();
            }
        });
    }

    private void initView(View view) {
        date = view.findViewById(R.id.date);
        btnCancel = view.findViewById(R.id.btnCancel);
        btnChoose = view.findViewById(R.id.btnChoose);
    }

    public void setDialogResult(OnMyDialogResult result) {
        dialogResult = result;
    }

    public interface OnMyDialogResult {
        void finish(String result, int dayOfMonthResult, int monthResult, int yearResult);
    }
}
