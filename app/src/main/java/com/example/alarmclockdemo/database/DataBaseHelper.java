package com.example.alarmclockdemo.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.alarmclockdemo.model.AlarmModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gcb Le Co.
 */
public class DataBaseHelper extends SQLiteOpenHelper {

    // Logcat tag
    private static final String LOG = "DatabaseHelper";

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "Alarm_Demo";

    // Table Names
    private static final String TABLE_NAME = "Alarm_Table";


    // Column names
    private static final String KEY_ID = "id";
    private static final String YEAR = "year";
    private static final String MONTH = "month";
    private static final String DAY = "dayOfMonth";
    private static final String HOUR = "hour";
    private static final String MINUTE = "minute";
    private static final String MESSAGE = "message";
    private static final String REPEAT_TIME = "repeatType";
    private static final String REPEAT_TYPE = "repeatTime";
    private static final String FLAG = "flag";
    private static final String DELETECHECK = "deleteCheck";

    // Table Create Statements
    private static final String CREATE_TABLE = "CREATE TABLE "
            + TABLE_NAME + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + YEAR + " INTEGER," + MONTH + " INTEGER," + DAY + " INTEGER,"
            + HOUR + " INTEGER," + MINUTE + " INTEGER,"
            + MESSAGE + " TEXT," + REPEAT_TIME + " INTEGER," + REPEAT_TYPE + " TEXT,"
            + FLAG + " INTEGER," + DELETECHECK + " INTEGER" + ")";

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        // create new tables
        onCreate(db);
    }

    public long insertAlarm(AlarmModel alarm) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(YEAR, alarm.getYear());
        values.put(MONTH, alarm.getMonth());
        values.put(DAY, alarm.getDayOfMonth());
        values.put(HOUR, alarm.getHour());
        values.put(MINUTE, alarm.getMinute());
        values.put(MESSAGE, alarm.getMessage());
        values.put(REPEAT_TIME, alarm.getRepeatTime());
        values.put(REPEAT_TYPE, alarm.getRepeatType());
        values.put(FLAG, alarm.getFlag());
        values.put(DELETECHECK, alarm.getDeleteCheck());

        // insert row
        long alarm_id = db.insert(TABLE_NAME, null, values);

        return alarm_id;
    }

    //    get single alarm
    public AlarmModel getAlarm(int alarm_id) {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_NAME + " WHERE "
                + KEY_ID + " = " + alarm_id;

        Cursor c = db.rawQuery(selectQuery, null);

        AlarmModel alarmModel = new AlarmModel();
        if (c != null && c.moveToFirst()) {
            alarmModel.setId(c.getInt(c.getColumnIndex(KEY_ID)));
            alarmModel.setYear(c.getInt(c.getColumnIndex(YEAR)));
            alarmModel.setMonth(c.getInt(c.getColumnIndex(MONTH)));
            alarmModel.setDayOfMonth(c.getInt(c.getColumnIndex(DAY)));
            alarmModel.setHour(c.getInt(c.getColumnIndex(HOUR)));
            alarmModel.setMinute(c.getInt(c.getColumnIndex(MINUTE)));
            alarmModel.setMessage(c.getString(c.getColumnIndex(MESSAGE)));
            alarmModel.setRepeatType(c.getString(c.getColumnIndex(REPEAT_TYPE)));
            alarmModel.setRepeatTime(c.getInt(c.getColumnIndex(REPEAT_TIME)));
            alarmModel.setFlag(c.getInt(c.getColumnIndex(FLAG)));
            alarmModel.setDeleteCheck(c.getInt(c.getColumnIndex(DELETECHECK)));
        }
        return alarmModel;
    }

    //    get all alarm
    public List<AlarmModel> getAlarmList() {
        List<AlarmModel> alarmList = new ArrayList<AlarmModel>();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                AlarmModel alarmItem = new AlarmModel();
                alarmItem.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                alarmItem.setYear(c.getInt(c.getColumnIndex(YEAR)));
                alarmItem.setMonth(c.getInt(c.getColumnIndex(MONTH)));
                alarmItem.setDayOfMonth(c.getInt(c.getColumnIndex(DAY)));
                alarmItem.setHour(c.getInt(c.getColumnIndex(HOUR)));
                alarmItem.setMinute(c.getInt(c.getColumnIndex(MINUTE)));
                alarmItem.setMessage(c.getString(c.getColumnIndex(MESSAGE)));
                alarmItem.setRepeatType(c.getString(c.getColumnIndex(REPEAT_TYPE)));
                alarmItem.setRepeatTime(c.getInt(c.getColumnIndex(REPEAT_TIME)));
                alarmItem.setFlag(c.getInt(c.getColumnIndex(FLAG)));
                alarmItem.setDeleteCheck(c.getInt(c.getColumnIndex(DELETECHECK)));
                // adding to alarm list
                alarmList.add(alarmItem);
            } while (c.moveToNext());
        }

        return alarmList;
    }

    //    get all alarm
    public List<Integer> getAlarmIdDelete() {
        List<Integer> idList= new ArrayList<Integer>();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME + " WHERE "+ DELETECHECK + " = " + 1;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                Integer id = 0;
                id = (c.getInt(c.getColumnIndex(KEY_ID)));
                idList.add(id);
            } while (c.moveToNext());
        }
        return idList;
    }

    public int updateAlarm(AlarmModel model) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        if (model.getYear() != null) {
            values.put(YEAR, model.getYear());
        }
        if (model.getMonth() != null) {
            values.put(MONTH, model.getMonth());
        }
        if (model.getDayOfMonth() != null) {
            values.put(DAY, model.getDayOfMonth());
        }
        if (model.getHour() != null) {
            values.put(HOUR, model.getHour());
        }
        if (model.getMinute() != null) {
            values.put(MINUTE, model.getMinute());
        }
        if (model.getMessage() != null) {
            values.put(MESSAGE, model.getMessage());
        }
        if (model.getRepeatType() != null) {
            values.put(REPEAT_TYPE, model.getRepeatType());
        }
        if (model.getRepeatTime() != null) {
            values.put(REPEAT_TIME, model.getRepeatTime());
        }
        if (model.getFlag() != null) {
            values.put(FLAG, model.getFlag());
        }
        if (model.getDeleteCheck() != null) {
            values.put(DELETECHECK, model.getDeleteCheck());
        }

        // updating row
        return db.update(TABLE_NAME, values, KEY_ID + " = ?",
                new String[]{String.valueOf(model.getId())});
    }

//    set choose item delete
    public boolean updateDeleteCheck(int check) {
        SQLiteDatabase db = this.getWritableDatabase();
        String where = "UPDATE "+ TABLE_NAME +" SET " + DELETECHECK + " = " + check;
        db.execSQL(where);
        return true;
    }

// delete alarm checked
    public void deleteAlarmChecked() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, DELETECHECK + " = " + 1, null);
    }
    public void deleteAlarm(long alarm_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, KEY_ID + " = ?",
                new String[]{String.valueOf(alarm_id)});
    }

    // closing database
    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }
}
