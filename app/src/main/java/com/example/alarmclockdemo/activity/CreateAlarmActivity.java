package com.example.alarmclockdemo.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.alarmclockdemo.common.Common;
import com.example.alarmclockdemo.database.DataBaseHelper;
import com.example.alarmclockdemo.dialog.CalendarDialog;
import com.example.alarmclockdemo.R;
import com.example.alarmclockdemo.dialog.CustomRepeatDialog;
import com.example.alarmclockdemo.model.AlarmModel;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.concurrent.atomic.AtomicInteger;

public class CreateAlarmActivity extends AppCompatActivity {
    private Button btnSave, btnCancel;
    private TextView tvDate;
    private LinearLayout lyDate;
    private ImageButton imgDate;
    private NumberPicker tvHour, tvMinute;
    private EditText edtMessage;
    private RadioGroup rbgRepeat;
    private RadioButton rbNever, rbHourly, rbDayly, rbWeekly, rbMonthly, rbYearly, rbCustom;
    private Calendar calendar = Calendar.getInstance();
    private FragmentManager fm;
    private int dayOfMonth, month, year;
    public static final String TAG = CreateAlarmActivity.class.getSimpleName(); //return: CreateAlarmActivity
    private static AtomicInteger count = new AtomicInteger(0);
    private int id;
    private String repeatType = "Never";
    private int repeatTime = 0;
    private DataBaseHelper db;
    private AlarmModel model = new AlarmModel();
    private String key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_alarm);
        fm = getSupportFragmentManager();
        db = new DataBaseHelper(getApplicationContext());
        Intent intent = getIntent();
        key = intent.getStringExtra("KEY");
        if (key == null) {
            key = "";
        }
        if (key.equals("UPDATE")) {
            model = intent.getParcelableExtra("MODEL");
        }
        initView();
        initData();
        initEvent();
    }

    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    private void initData() {
        format(tvHour);
        tvHour.setMaxValue(23);
        tvHour.setMinValue(0);
        format(tvMinute);
        tvMinute.setMaxValue(59);
        tvMinute.setMinValue(0);

        if (key.equals("UPDATE")) {
            tvHour.setValue(model.getHour());
            tvMinute.setValue(model.getMinute());
            year = model.getYear();
            month = model.getMonth();
            dayOfMonth = model.getDayOfMonth();
            formatDayOfWeek();
            repeatTime = model.getRepeatTime();
            repeatType = model.getRepeatType();
            if (repeatType.equals("Munite")) {
                rbCustom.setChecked(true);
            } else if (repeatTime <= 1) {
                switch (repeatType) {
                    case "Never":
                        rbNever.setChecked(true);
                        break;
                    case "Hour":
                        rbHourly.setChecked(true);
                        break;
                    case "Day":
                        rbDayly.setChecked(true);
                        break;
                    case "Week":
                        rbWeekly.setChecked(true);
                        break;
                    case "Month":
                        rbMonthly.setChecked(true);
                        break;
                    case "Year":
                        rbYearly.setChecked(true);
                        break;
                    default:
                        rbCustom.setChecked(true);
                }
            } else if (repeatTime > 1) {
                rbCustom.setChecked(true);
            }
        } else {
            tvHour.setValue(calendar.get(Calendar.HOUR_OF_DAY));
            tvMinute.setValue(calendar.get(Calendar.MINUTE) + 1);
            dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
            month = calendar.get(Calendar.MONTH);
            year = calendar.get(Calendar.YEAR);
            formatDayOfWeek();
        }
    }

    @SuppressLint("WrongConstant")
    private void formatDayOfWeek() {
        String day = "";
        String dateParse = year + "-" + String.format("%02d", month + 1) + "-" + String.format("%02d", dayOfMonth);
        LocalDate convert = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            convert = LocalDate.parse(dateParse);
            DayOfWeek dayOfWeek = convert.getDayOfWeek();
            if (String.valueOf(dayOfWeek).equals("MONDAY")) day = "T.2";
            else if (String.valueOf(dayOfWeek).equals("TUESDAY")) day = "T.3";
            else if (String.valueOf(dayOfWeek).equals("WEDNESDAY")) day = "T.4";
            else if (String.valueOf(dayOfWeek).equals("THURSDAY")) day = "T.5";
            else if (String.valueOf(dayOfWeek).equals("FRIDAY")) day = "T.6";
            else if (String.valueOf(dayOfWeek).equals("SATURDAY")) day = "T.7";
            else if (String.valueOf(dayOfWeek).equals("SUNDAY")) day = "CN";
            String dateText
                    = day + ", "
                    + String.format("%02d", dayOfMonth) + "Th"
                    + String.format("%02d", month + 1);
            tvDate.setText(dateText);
        }
    }

    private void initView() {
        btnSave = findViewById(R.id.btnSave);
        btnCancel = findViewById(R.id.btnCancel);
        tvDate = findViewById(R.id.tvDate);
        lyDate = findViewById(R.id.lyDate);
        imgDate = findViewById(R.id.imgDate);
        tvHour = findViewById(R.id.tvHour);
        tvMinute = findViewById(R.id.tvMinute);
        edtMessage = findViewById(R.id.edtMessage);
        rbgRepeat = findViewById(R.id.rbgRepeat);
        rbNever = findViewById(R.id.rbNever);
        rbHourly = findViewById(R.id.rbHourly);
        rbDayly = findViewById(R.id.rbDayly);
        rbWeekly = findViewById(R.id.rbWeekly);
        rbMonthly = findViewById(R.id.rbMonthly);
        rbYearly = findViewById(R.id.rbYearly);
        rbCustom = findViewById(R.id.rbCustom);
    }

    private void initEvent() {
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addAlarm();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                setResult(MainActivity.RESULT_CANCELED, returnIntent);
                finish();
            }
        });

        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseDate();
            }
        });

        lyDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseDate();
            }
        });

        imgDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseDate();
            }
        });

        rbgRepeat.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rbNever:
                        repeatType = "Never";
                        repeatTime = 1;
                        break;
                    case R.id.rbHourly:
                        repeatType = "Hour";
                        repeatTime = 1;
                        break;
                    case R.id.rbDayly:
                        repeatType = "Day";
                        repeatTime = 1;
                        break;
                    case R.id.rbWeekly:
                        repeatType = "Week";
                        repeatTime = 1;
                        break;
                    case R.id.rbMonthly:
                        repeatType = "Month";
                        repeatTime = 1;
                        break;
                    case R.id.rbYearly:
                        repeatType = "Year";
                        repeatTime = 1;
                        break;
                    case R.id.rbCustom:
                        repeatType = "Custom";
                        customRepeatTime();
                        break;
                }
            }
        });
    }

    private void format(NumberPicker v) {
        v.setFormatter(new NumberPicker.Formatter() {
            @SuppressLint("DefaultLocale")
            @Override
            public String format(int i) {
                return String.format("%02d", i);
            }
        });
    }

    private void addAlarm() {
        String message = edtMessage.getText().toString();
        if (message.isEmpty()) {
            model.setMessage("Báo thức");
        } else {
            model.setMessage(message);
        }
        model.setHour(tvHour.getValue());
        model.setMinute(tvMinute.getValue());
        model.setYear(year);
        model.setMonth(month);
        model.setDayOfMonth(dayOfMonth);
        model.setRepeatType(repeatType);
        model.setRepeatTime(repeatTime);
        model.setFlag(1);
        model.setDeleteCheck(0);
        if (key.equals("UPDATE")) {
            db.updateAlarm(model);
            Common.setAlarm(getApplicationContext(), model.getId());
        } else if (key.equals("CREATE")) {
            int id = (int) db.insertAlarm(model);
            Common.setAlarm(getApplicationContext(), id);
        }
        setResult(MainActivity.RESULT_OK);
        finish();
    }

    private void chooseDate() {
        CalendarDialog dialog = new CalendarDialog();
        dialog.show(fm, "");
        dialog.setDialogResult(new CalendarDialog.OnMyDialogResult() {
            @Override
            public void finish(String result, int dayOfMonthResult, int monthResult, int yearResult) {
                tvDate.setText(result);
                year = yearResult;
                month = monthResult;
                dayOfMonth = dayOfMonthResult;
            }
        });
    }

    private void customRepeatTime() {
        CustomRepeatDialog dialog = new CustomRepeatDialog();
        dialog.show(fm, "");
        dialog.setDialogResult(new CustomRepeatDialog.OnMyDialogResult() {
            @Override
            public void finish(int time, String repeatTypeResul) {
                repeatTime = time;
                repeatType = repeatTypeResul;
            }
        });
    }
}

