package com.example.alarmclockdemo.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.alarmclockdemo.R;

//import android.app.AlertDialog;
//import android.content.DialogInterface;
//import android.content.Intent;
//import com.example.alarmclockdemo.common.Common;
//import com.example.alarmclockdemo.model.AlarmModel;
public class DialogActivity extends AppCompatActivity {
//    private AlarmModel model;
//    private String title;
//
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);
//
//        Intent intent = getIntent();
//        Bundle bundle = intent.getBundleExtra("ALARM_INTENT");
//        if (bundle != null) {
//            model = bundle.getParcelable("ALARM_BUNDLE");
//        }
//        if (model != null) {
//            title = String.format("%02d", model.getHour()) + ":" + String.format("%02d", model.getMinute());
//            displayAlert();
//        }
//    }
//
////    private void showNotification() {
////
////        title = String.format("%02d", model.getHour()) + ":" + String.format("%02d", model.getMinute());
////
////        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
////        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
////            NotificationChannel channel = new NotificationChannel("YOUR_CHANNEL_ID",
////                    "" + "YOUR_CHANNEL_NAME",
////                    NotificationManager.IMPORTANCE_LOW);
////            channel.setDescription("YOUR_NOTIFICATION_CHANNEL_DISCRIPTION");
////            mNotificationManager.createNotificationChannel(channel);
////        }
////
////        Intent intent = new Intent(getApplicationContext(), DialogActivity.class);
////        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
////        PendingIntent pi = PendingIntent.getActivity(this, model.getId(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
////
////        Intent snoozeIntent = new Intent(this, AlarmReceiver.class);
////        snoozeIntent.setAction(Intent.ACTION_SCREEN_ON);
////        snoozeIntent.putExtra(NOTIFICATION_SERVICE, 0);
////        PendingIntent snoozePendingIntent = PendingIntent.getBroadcast(this,  model.getId(), snoozeIntent, 0);
////        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext(), "YOUR_CHANNEL_ID")
////                .setSmallIcon(R.drawable.ic_alarm) // notification icon
////                .setContentTitle(title) // title for notification
////                .setContentText(model.getMessage())// message for notification
////                .setAutoCancel(true) // clear notification after click
////                .setContentIntent(pi)
////                .addAction(R.drawable.ic_add, getString(R.string.close),
////                        snoozePendingIntent);
////        mBuilder.setContentIntent(pi);
////        mNotificationManager.notify( model.getId(), mBuilder.build());
////        mBuilder.getNotification();
////    }
//
//    private void displayAlert() {
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle(title)
//                .setMessage(model.getMessage())
//                .setCancelable(false)
//                .setPositiveButton("Tắt", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        cancelDialog(dialog);
//                    }
//                })
//                .setNegativeButton("Báo lại", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        cancelDialog(dialog);
//                        model.setMinute(model.getMinute() + 5);
//                        Common.setAlarm(DialogActivity.this, model);
//                    }
//                });
//        AlertDialog alertDialog = builder.create();
//        alertDialog.show();
//    }
//
//    private void cancelDialog(DialogInterface dialog) {
////        CommonUtils.cancelAlarm(DialogActivity.this, model.getId());
//        Common.stopMediaPlayer();
//        Common.cancelNotification(this, model.getId());
//        dialog.cancel();
//        DialogActivity.this.finish();
//        System.exit(0);
//        dialog.cancel();
//        finish();
    }
}
