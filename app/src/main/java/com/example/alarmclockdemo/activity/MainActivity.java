package com.example.alarmclockdemo.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alarmclockdemo.R;
import com.example.alarmclockdemo.adapter.AlarmAdapter;
import com.example.alarmclockdemo.common.Common;
import com.example.alarmclockdemo.database.DataBaseHelper;
import com.example.alarmclockdemo.model.AlarmModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {
    private RecyclerView rvAlarm;
    private ImageButton imgDelete;
    private TextView tvCancel, tvDelete;
    private CheckBox chkCheckAll;
    private LinearLayout lyDeleteMenu, lyCheckAll;
    private AlarmAdapter alarmAdapter;
    private List<AlarmModel> data = new ArrayList<>();
    private DataBaseHelper db;
    private int REQUEST_CODE_CREATE_ALARM = 2000;
    private FloatingActionButton btnCreate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = new DataBaseHelper(getApplicationContext());
        data = db.getAlarmList();
        initView();
        initEvent();
    }

    private void initView() {
        rvAlarm = findViewById(R.id.rvAlarm);
        imgDelete = findViewById(R.id.imgDelete);
        lyDeleteMenu = findViewById(R.id.lyDeleteMenu);
        lyDeleteMenu.setVisibility(View.GONE);
        lyCheckAll = findViewById(R.id.lyCheckAll);
        lyCheckAll.setVisibility(View.GONE);
        tvDelete = findViewById(R.id.tvDelete);
        tvCancel = findViewById(R.id.tvCancel);
        chkCheckAll = findViewById(R.id.chkCheckAll);
        chkCheckAll.setChecked(false);
        btnCreate = findViewById(R.id.btnCreate);
        LinearLayoutManager linearLayoutRecycleView = new LinearLayoutManager(this);
        rvAlarm.setLayoutManager(linearLayoutRecycleView);
        alarmAdapter = new AlarmAdapter(this, data, false);
        rvAlarm.setAdapter(alarmAdapter);
        if (data.size() > 0) {
            imgDelete.setVisibility(View.VISIBLE);
        }
    }

    private void initEvent() {
        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CreateAlarmActivity.class);
                intent.putExtra("KEY", "CREATE");
                startActivityForResult(intent, REQUEST_CODE_CREATE_ALARM);
            }
        });

        imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideView("SHOW_DELETE");
            }
        });

        tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Integer> idList = db.getAlarmIdDelete();
                for (int id : idList) {
                    Common.cancelAlarm(getApplicationContext(), id);
                }
                db.deleteAlarmChecked();
                hideView("DELETE");
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.updateDeleteCheck(0);
                hideView("CANCEL");
            }
        });

        chkCheckAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chkCheckAll.isChecked()) {
                    db.updateDeleteCheck(1);
                } else {
                    db.updateDeleteCheck(0);
                }
                refreshData(true);
            }
        });

        alarmAdapter.setClickListener(new AlarmAdapter.OnItemClickListener() {
            @Override
            public void onItemAdapterClick(AlarmModel model) {
                Toast.makeText(MainActivity.this, "Adapter click", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void refreshData(boolean checkBoxShow) {
        data = db.getAlarmList();
        alarmAdapter = new AlarmAdapter(MainActivity.this, data, checkBoxShow);
        rvAlarm.setAdapter(alarmAdapter);
        alarmAdapter.notifyDataSetChanged();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint("DefaultLocale")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == REQUEST_CODE_CREATE_ALARM && resultCode == RESULT_OK) {
            data = db.getAlarmList();
            alarmAdapter = new AlarmAdapter(this, data, false);
            rvAlarm.setAdapter(alarmAdapter);
            alarmAdapter.notifyDataSetChanged();
            if (data.size() > 0) {
                imgDelete.setVisibility(View.VISIBLE);
            } else {
                imgDelete.setVisibility(View.GONE);
            }
        }
    }

    private void hideView(String type) {
        if (type.equals("SHOW_DELETE")) {
            imgDelete.setVisibility(View.GONE);
            btnCreate.setVisibility(View.GONE);
            lyCheckAll.setVisibility(View.VISIBLE);
            lyDeleteMenu.setVisibility(View.VISIBLE);
            lyCheckAll.setVisibility(View.VISIBLE);
            chkCheckAll.setChecked(false);
            rvAlarm.setEnabled(false);
            refreshData(true);
        } else if (type.equals("CANCEL") || type.equals("DELETE")) {
            if (data.size() > 0) {
                imgDelete.setVisibility(View.VISIBLE);
            } else {
                imgDelete.setVisibility(View.GONE);
            }
            rvAlarm.setEnabled(true);
            btnCreate.setVisibility(View.VISIBLE);
            lyCheckAll.setVisibility(View.GONE);
            lyDeleteMenu.setVisibility(View.GONE);
            lyCheckAll.setVisibility(View.GONE);
            chkCheckAll.setChecked(false);
            refreshData(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        this.registerReceiver(refreshSwitch, new IntentFilter("REFRESH_SWITCH"));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(refreshSwitch);
    }

//    //    refresh alarm after click switch
//    private BroadcastReceiver refreshAlarm = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            AlarmModel model = intent.getParcelableExtra("MODEL");
//            if (model.getFlag() == 1) {
//                Common.setAlarm(getApplicationContext(), model.getId());
//            } else {
//                Common.cancelAlarm(getApplicationContext(), model.getId());
//            }
//            refreshData(false);
//        }
//    };

    //    refresh alarm after click cancel button action
    private BroadcastReceiver refreshSwitch = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            refreshData(false);
        }
    };
}
