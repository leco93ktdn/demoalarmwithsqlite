package com.example.alarmclockdemo.alarm_receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.alarmclockdemo.common.Common;

public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action != null && !action.equals("")) {
            int id = intent.getIntExtra("ID", 0);
            String uri = intent.getStringExtra("KEY_TONE_URL");
            Log.e("ACTION", " " + action);
            Log.e("ALARM ID", " " + id);
            if (action.equals("ACTION_ALARM")) {
                Log.e("URI", " " + uri);
                if (uri != null) {
                    Common.startMediaPlayer(context.getApplicationContext(), uri);
                }
                Common.showNotification(context.getApplicationContext(), id);
            }
        }
    }
}